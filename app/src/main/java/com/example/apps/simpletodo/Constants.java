package com.example.apps.simpletodo;

public class Constants {
    // key variable declarations

    // request codes
    public static final class ReqCode {
        public static final int newItem = 0;
    }

    // list item
    public static final class ListItem {
        public static final String EMPTY = "empty_string";
    }

    // view type
    public static final class ViewType {
        public static final int EMPTY = 1;
        public static final int ITEM = 0;
    }
}
