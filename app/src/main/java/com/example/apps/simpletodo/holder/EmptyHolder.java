package com.example.apps.simpletodo.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public class EmptyHolder extends RecyclerView.ViewHolder {
    // generated constructor
    public EmptyHolder(View itemView) {
        super(itemView);
    }
}
