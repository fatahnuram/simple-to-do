package com.example.apps.simpletodo.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.apps.simpletodo.Constants;
import com.example.apps.simpletodo.R;
import com.example.apps.simpletodo.holder.EmptyHolder;
import com.example.apps.simpletodo.holder.ItemHolder;
import com.example.apps.simpletodo.models.ItemModel;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter {
    // variable declarations
    private List items;
    private Context context;
    private View view;

    // custom constructor
    public RecyclerAdapter(Context context, List items){
        // get passed args
        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // check view type
        if(viewType == Constants.ViewType.ITEM){
            // inflate item holder
            view = LayoutInflater.from(context).inflate(R.layout.board_item, parent, false);
            return new ItemHolder(view, context);
        }else {
            // inflate empty holder
            view = LayoutInflater.from(context).inflate(R.layout.board_kosong, parent, false);
            return new EmptyHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        // check object type
        if(holder instanceof ItemHolder){
            // get item object
            ItemModel model = (ItemModel) items.get(position);
            // bind holder
            ((ItemHolder) holder).bind(model);
        }
    }

    @Override
    public int getItemCount() {
        // inflate as much as item list size
        return items.size();
    }

    // override item view type
    @Override
    public int getItemViewType(int position){
        // inflate item type if list have more than 1 items
        if(items.size() > 1){
            return Constants.ViewType.ITEM;
        }else {
            // check object type if list only have 1 item
            if(items.get(position) instanceof ItemModel){
                // inflate item type if item object
                return Constants.ViewType.ITEM;
            }else {
                // inflate empty type if empty object
                return Constants.ViewType.EMPTY;
            }
        }
    }

    // reload data
    public final void reloadData(List items){
        // show debug log
        Log.d("custom log", "Loading data..");
        // check if null
        if(items != null){
            this.items = items;
        }
        // reload adapter data
        notifyDataSetChanged();
    }
}
