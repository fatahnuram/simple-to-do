package com.example.apps.simpletodo.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apps.simpletodo.R;
import com.example.apps.simpletodo.models.ItemModel;

public class AddActivity extends AppCompatActivity {
    // variable declarations
    private Button btnAdd;
    private EditText etName;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        initViews();
    }

    // init views
    private void initViews(){
        btnAdd = findViewById(R.id.add_btn_add);
        etName = findViewById(R.id.add_et_name);
        // set button function
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get string value from input
                name = etName.getText().toString();
                // test name not null
                assert name != null;
                // check length
                if(name.length() == 0){
                    // test name length
                    assert name.length() == 0;
                    // warning text
                    String warning = "Item name must not be empty";
                    // show warning
                    Toast.makeText(AddActivity.this, warning, Toast.LENGTH_SHORT).show();
                    // show debug log
                    Log.d("custom warning", warning);
                }else{
                    // test name length
                    assert name.length() > 0;
                    // save item to database
                    ItemModel model = new ItemModel(name);
                    ItemModel.save(model);
                    // set result to trigger main activity to reload data
                    setResult(RESULT_OK);
                    // show debug log
                    Log.d("custom log", "Added item: " + name);
                    // close activity
                    finish();
                }
            }
        });
    }

    // show cancel alert when any string exist
    @Override
    public void onBackPressed(){
        // check input
        if(etName.getText().toString().length() > 0){
            // show confirmation dialog
            new AlertDialog.Builder(AddActivity.this)
                    .setTitle("Cancel confirmation")
                    .setMessage("Are you sure want to discard item?")
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // close dialog
                            dialog.dismiss();
                            // show debug log
                            Log.d("custom log", "Cancelled confirmation");
                        }
                    })
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setResult(RESULT_CANCELED);
                            // show debug log
                            Log.d("custom log", "Cancelled adding item");
                            // close activity
                            finish();
                        }
                    })
                    .show();
        }else {
            // show debug log
            Log.d("custom log", "Cancelled adding item");
            // finish activity if input is none
            finish();
        }
    }
}
