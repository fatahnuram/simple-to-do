package com.example.apps.simpletodo.event;

import com.example.apps.simpletodo.models.ItemModel;

public class DialogEvent {
    // variable declarations
    private ItemModel model;

    // custom constructor
    public DialogEvent(ItemModel model){
        // get item
        this.model = model;
    }

    // getter
    public ItemModel getModel() {
        return model;
    }
}
