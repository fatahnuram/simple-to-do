package com.example.apps.simpletodo.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.apps.simpletodo.Constants;
import com.example.apps.simpletodo.R;
import com.example.apps.simpletodo.adapter.RecyclerAdapter;
import com.example.apps.simpletodo.event.DialogEvent;
import com.example.apps.simpletodo.models.ItemModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    // variable declarations
    private List list;
    private Button btnAdd;
    private RecyclerView rvItems;
    private RecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkSavedData();
        initViews();
        loadData(list);
    }

    // check database for items saved
    private void checkSavedData(){
        // get saved items count
        long num = ItemModel.count(ItemModel.class);
        // show debug log
        Log.d("custom log", "Found data: " + num);
        // check if any items saved
        if(num > 0){
            // get all items
            list = ItemModel.listAll(ItemModel.class);
            // test list item
            assert list.get(0) instanceof ItemModel;
        }else {
            // init empty view
            list = new ArrayList();
            list.add(Constants.ListItem.EMPTY);
            // test list item
            assert list.get(0) instanceof String;
        }
    }

    // init views
    private void initViews(){
        btnAdd = findViewById(R.id.main_btn_add);
        rvItems = findViewById(R.id.main_recycler_items);
        // set button function
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // intent to add activity
                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                startActivityForResult(intent, Constants.ReqCode.newItem);
                // show debug log
                Log.d("custom log", "Moved to add activity");
            }
        });
    }

    // load data into recycler
    private void loadData(List list){
        // create new adapter
        adapter = new RecyclerAdapter(MainActivity.this, list);
        // set adapter etc
        rvItems.setAdapter(adapter);
        rvItems.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        rvItems.setItemAnimator(new DefaultItemAnimator());
    }

    // handle results from activity
    @Override
    public void onActivityResult(int reqCode, int resCode, Intent data){
        // check request code
        if(reqCode == Constants.ReqCode.newItem){
            // check result code
            if(resCode == RESULT_OK){
                // get all data
                checkSavedData();
                // reload data
                adapter.reloadData(list);
            }
        }
    }

    // register eventbus
    @Override
    public void onStart(){
        super.onStart();
        EventBus.getDefault().register(MainActivity.this);
    }

    // unregister eventbus
    @Override
    public void onStop(){
        super.onStop();
        EventBus.getDefault().unregister(MainActivity.this);
    }

    // eventbus handling
    @Subscribe
    public void onEvent(DialogEvent event){
        // delete item from database
        showDeleteConfirmation(event.getModel());
    }

    // show delete confirmation dialog
    private void showDeleteConfirmation(final ItemModel model){
        // show debug log
        Log.d("custom log", "Delete confirmation");
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Delete item")
                .setMessage("Are you sure want to delete this item?")
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // close dialog
                        dialog.dismiss();
                        // show debug log
                        Log.d("custom log", "Cancelled confirmation");
                    }
                })
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // delete item from database
                        ItemModel.delete(model);
                        // show debug log
                        Log.d("custom log", "Data deleted");
                        // reload data
                        checkSavedData();
                        adapter.reloadData(list);
                    }
                })
                .show();
    }
}
