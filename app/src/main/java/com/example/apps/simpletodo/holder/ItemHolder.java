package com.example.apps.simpletodo.holder;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.example.apps.simpletodo.R;
import com.example.apps.simpletodo.event.DialogEvent;
import com.example.apps.simpletodo.models.ItemModel;

import org.greenrobot.eventbus.EventBus;

public class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    // variable declarations
    private CheckBox cbItem;
    private ImageView ivClose;
    private Context context;
    private ItemModel model;

    // custom constructor
    public ItemHolder(View itemView, Context context) {
        super(itemView);
        // get context
        this.context = context;
        // init views
        cbItem = itemView.findViewById(R.id.item_name);
        ivClose = itemView.findViewById(R.id.item_close);
    }

    // bind
    public void bind(ItemModel model){
        // get passed item
        this.model = model;
        // set item name
        cbItem.setText(model.getName());
        toggleChecked(model);
        // set button functions
        ivClose.setOnClickListener(this);
        cbItem.setOnClickListener(this);
    }

    // delete data from database
    private void deleteData(final ItemModel model){
        // trigger main activity to show confirmation
        EventBus.getDefault().post(new DialogEvent(model));
    }

    // toggle item checked
    private void toggleChecked(ItemModel model){
        // check if checked
        if(model.isChecked()){
            // make text strike through
            cbItem.setPaintFlags(cbItem.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            cbItem.setTextColor(Color.parseColor("#BDBDBD"));
            // show debug log
            Log.d("custom log", "Item done");
        }else {
            // remove strike through from text
            cbItem.setPaintFlags(cbItem.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            cbItem.setTextColor(Color.parseColor("#000000"));
            // show debug log
            Log.d("custom log", "Item not done");
        }
        cbItem.setChecked(model.isChecked());
    }

    // define button functions
    @Override
    public void onClick(View v) {
        // check view id
        if(v.getId() == R.id.item_name){
            // update database
            model.setChecked(cbItem.isChecked());
            model.save();
            // toggle view
            toggleChecked(model);
        }else if(v.getId() == R.id.item_close){
            deleteData(model);
        }
    }
}
