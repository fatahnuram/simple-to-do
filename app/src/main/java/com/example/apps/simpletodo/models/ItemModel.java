package com.example.apps.simpletodo.models;

import com.orm.SugarRecord;

import java.util.Calendar;

public class ItemModel extends SugarRecord {
    // variable declarations
    private String name;
    private boolean isChecked;
    private long created;

    // default constructor
    public ItemModel(){
        created = Calendar.getInstance().getTimeInMillis();
    }

    // custom constructor
    public ItemModel(String name){
        this.name = name;
        created = Calendar.getInstance().getTimeInMillis();
    }

    // getter setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }
}
